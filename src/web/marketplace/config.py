import os

def env(key, default=None):
    return os.environ.get(key, default)

DEV_DB = 'sqlite:///marketplace.db'

pg_user=env('POSTGRES_USER', 'admin')
pg_pass=env('POSTGRES_PASS', 'admin')
pg_db=env('POSTGRES_DB', 'marketplace')
pg_host=env('POSTGRES_HOST', 'db')
pg_port=int(env('POSTGRES_PORT', "5432"))

PROD_DB = f'postgresql://{pg_user}:{pg_pass}@{pg_host}:{pg_port}/{pg_db}'
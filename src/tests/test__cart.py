from flask import session
from utils import get_mockdata, get_cart_mock, api_login, api_register
from bs4 import BeautifulSoup

def test_cart(app):
    user_mock = get_mockdata()
    client = app.test_client()
    mock = get_cart_mock()

    with client:
        # login user
        api_register(user_mock, client)
        api_login(user_mock, client)

        # add product to cart
        client.post('/addcart', data=mock)

        assert session.get('cart') != None, "No cart found (session is None)"
        assert len(session.get('cart')) > 0, "No cart found, session is empty"

        ids = session.get('cart').keys()
        assert len(ids) == 1, "Cart is not empty"
        assert mock['product_id'] in ids, "Product id is not the expected one"
        assert str(session.get('cart')[mock['product_id']]['quantity']) == str(mock['quantity']), "Quantity is not the expected one"

        # check if product is in the cart
        cart_list = client.get('/carts')

        html = BeautifulSoup(cart_list.text, 'html.parser')
        data = html.find('table').find('tbody').find('tr').find_all('td')

        # # there is 7 columns in the table
        # # the first tr is the latest added category
        assert len(data) == 7, "There is not 7 columns in the table"
        assert str(data[3].text) == str(mock['quantity']), "Quantity is not the expected one"


        # delete product from cart
        client.get('/deleteitem/{}'.format(mock['product_id']))

        assert session.get('cart') != None, "No cart found (session is None)"
        assert len(session.get('cart')) == 0, "Cart is not empty"
        



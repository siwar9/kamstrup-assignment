from flask import session
from utils import get_mockdata, get_category_mock, check_flashes, api_login, api_register
from bs4 import BeautifulSoup

def test_adding_category(app):
    user_mock = get_mockdata()
    client = app.test_client()
    mock = get_category_mock()
    with client:
        # login user
        api_register(user_mock, client)
        api_login(user_mock, client)

        # add category
        client.post('/addcategory', data=mock)
        expected_message = "The category {} was added successfully!".format(mock['category'])

        # check if category is in the list
        categories_list = client.get('/categories', data=mock)

        html = BeautifulSoup(categories_list.text, 'html.parser')

        data = html.find('table').find('tbody').find('tr').find_all('td')

        # there is 4 columns in the table
        # the first tr is the latest added category
        assert len(data) == 4, "There is not 4 columns in the table"
        assert data[1].text == mock['category'], "Category is not the expected one"
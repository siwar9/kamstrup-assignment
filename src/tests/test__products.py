from flask import session
from utils import get_mockdata, get_product_mock, check_flashes, api_login, api_register
from bs4 import BeautifulSoup
import requests

def test_adding_products(app):
    user_mock = get_mockdata()
    client = app.test_client()
    mock = get_product_mock()

    with client:
        # login user
        api_register(user_mock, client)
        api_login(user_mock, client)

        # add product, in case of no image
        client.post('/addproduct', data=mock)
        expected_message_in_case_of_no_file = "No file part"

        check_flashes(session.get('_flashes'), expected_message_in_case_of_no_file, 'message')

        # add product with image
        mock_image_name = 'coca-cola-classic.jpg'
        mock_image_url = "https://www.myamericanmarket.com/26686-large_default/coca-cola-classic.jpg"
        mock_image = requests.get(mock_image_url, stream=True).raw
        mock['file'] = (mock_image, mock_image_name)
        expected_message = "The product {} was added successfully!".format(mock['name'])
        client.post('/addproduct', data=mock)
        check_flashes(session.get('_flashes'), expected_message)

        # check if category is in the list
        products_list = client.get('/admin', data=mock)

        html = BeautifulSoup(products_list.text, 'html.parser')
        data = html.find('table').find('tbody').find_all('tr')[-1].find_all('td')

        # there is 7 columns in the table
        # the first tr is the latest added category
        assert len(data) == 7, "There is not 7 columns in the table"
        assert data[1].text == mock['name'], "Product name is not the expected one"
import pytest

def import_app():
    import sys
    sys.path.append('../web')
    from marketplace import app 
    return app

@pytest.fixture()
def app():
    server = import_app()
    server.config.update({
        "TESTING": True,
    })
    yield server

@pytest.fixture()
def client(app):
    return app.test_client()


@pytest.fixture()
def runner(app):
    return app.test_cli_runner()
    
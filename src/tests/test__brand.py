from flask import session
from utils import get_mockdata, get_brand_mock, check_flashes, api_login, api_register
from bs4 import BeautifulSoup

def test_adding_brand(app):
    user_mock = get_mockdata()
    client = app.test_client()
    mock = get_brand_mock()
    with client:
        # login user
        api_register(user_mock, client)
        api_login(user_mock, client)

        # add brand
        client.post('/addbrand', data=mock)
        expected_message = "The brand {} was added successfully!".format(mock['brand'])
        check_flashes(session.get('_flashes'), expected_message)

        # check if brand is in the list
        brands_list = client.get('/brands', data=mock)

        html = BeautifulSoup(brands_list.text, 'html.parser')

        data = html.find('table').find('tbody').find('tr').find_all('td')

        # there is 4 columns in the table
        # the first tr is the latest added brand
        assert len(data) == 4, "There is not 4 columns in the table"
        assert data[1].text == mock['brand'], "Brand is not the expected one"
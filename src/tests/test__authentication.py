from flask import session
from utils import get_mockdata, check_flashes, api_login, api_register

def test_register_and_login(app):
    mock = get_mockdata()
    client = app.test_client()
    with client:
        api_register(mock, client)
        expected_message = "Welcome {}, thank you for registering!".format(mock['name'])
        check_flashes(session.get('_flashes'), expected_message)

        api_login(mock, client)

        assert session.get('email') == mock['email'], "User is not logged in"

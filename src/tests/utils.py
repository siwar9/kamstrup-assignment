from random import randint

def random_text(length):
    return ''.join([chr(randint(ord('a'), ord('z'))) for i in range(length)])

def get_mockdata():
    txt = random_text(10)
    mock = {
        "name": "John doe",
        "username": "john.doe." + txt,
        "email": "john." + txt + "@doe.com",
        "password": "123456",
        "confirm": "123456"
    }

    return mock

def get_brand_mock():
    txt = random_text(10)
    mock = {
        "brand": txt + " Inc.", 
    }

    return mock

def get_category_mock():
    txt = random_text(10)
    mock = {
        "category": txt, 
    }

    return mock


def get_product_mock():
    txt = random_text(10)
    mock = {
        "name": txt,
        "price": str(randint(1, 100)),
        "stock": str(randint(1, 10)),
        "brand": "1",
        "category": "1", 
    }

    return mock

def get_cart_mock():
    mock = {
        "product_id": "1",
        "quantity": str(randint(1, 10)),
    }

    return mock

def check_flashes(session, expected_message, expected_type='success'):
    assert session != None, "No flashes found (session is None)"
    assert len(session) > 0, "No flashes found, session is empty"

    t, m = session[-1]
    assert t == expected_type, "Flash type is not o type '{}'".format(expected_type)
    assert m == expected_message, "Flash message is not the expected one"


def api_register(mock, client):
    client.post('/register', data=mock)

def api_login(mock, client):
    client.post('/login', data={
        "email": mock["email"],
        "password": mock["password"]
    })


## Deployment on a cloud provider ☁️

**Running app on AKS:** http://20.26.169.252/

With Microsoft Azure for Students, I get a $100 credit available for 12 months in my Microsoft Azure account that is associated with the Tunisian Ministry of Higher Education.


To better demonstrate my working web app and to make sure that my database is indeed persistent, I have decided to deploy it using AKS or Azure Kubernetes Service.

## Step 1: Create the cluster on AKS

I used Azure Portal to create my cluster `kamstrup-cluster` in my `kamstrup` resource. I used nothing fancy; my cluster has 2 worker nodes and uses `Calico` for the PodNetwork. I also enabled HTTP routing.

For specifics on the cluster, please go to the cluster folder where I put the cluster template.

## Step 2: Authenticate to the cluster

To be able to use the `kubectl` command line normally, I have to first run this command in my cloud shell:
```bash
siwar@Azure:~$ az aks get-credentials --resource-group kamstrup --name kamstrup-cluster
```
And now it's time to create the manifest files.

## Step 3: Create configMap and secrets

To be able to inject my configuration variables into my containers in a secure way, I can use Kubernetes' configMaps and Secrets. Later, I can import the configMap and Secrets manifest in my deployment.

configMap.yaml:
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: config-map
data:
  web-port: "5000"
  db-host: db-svc
  db-port: "5432"
  db-name: Marketplace
  debug: "0"
```

Here, I set the value to the web-port (5000 because it is a flask app), the db-host:`db-svc` which is a reference to the database service in the postgres-svc.yaml manifest, the db-port(5432 since I am using PostgreSQL), the name of the database, and the debug environment variable (equals to 0 when in prod mode and 1 in dev mode).

As for the secrets.yaml:
```yaml
apiVersion: v1
kind: Secret
metadata:
  name: secrets
type: Opaque
data:
  secret: em8yaWhjMmNoanM5MzRib2FlaWc1ZWNpb3g4amVu
  db-username: YWRtaW4=
  db-password: YWRtaW4=

```
I used a random secret and for the db-username and db-password values are both `admin`encoded in base64 using the command:
```bash
siwar@Azure:~$ echo -n "admin" | base64
YWRtaW4=
```
## Step 4: Create persistent volume in AKS

I followed the Microsoft Azure Official Documentation on how to create a persistent volume: *[Dynamically create and use a persistent volume with Azure Disks in Azure Kubernetes Service (AKS)](https://learn.microsoft.com/en-us/azure/aks/azure-disks-dynamic-pv?fbclid=IwAR0bFpZ41_k1Ru7ysvP1XQD4fq00JVO6Zf4CvywwewmllKxRNe7iI0iz5QA)*

and then I created the persistent volume claim to use of the pre-existing storage classes (`managed-csi`) in my azure subscription.

pvc-disk.yaml:
```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: azure-managed-disk
spec:
  accessModes:
  - ReadWriteOnce
  storageClassName: managed-csi
  resources:
    requests:
      storage: 5Gi

```
## Step 4: Create the deployments and services

### Services

For the database, I have made a `ClusterIP` service just to expose my database pod internally.

postgres-svc.yaml:
```yaml
apiVersion: v1
kind: Service
metadata:
  name: db-svc
spec:
  selector:
    app: postgres-sts
  type: ClusterIP
  ports:
    - protocol: TCP
      port: 5432
      targetPort: 5432

```
Here I used the same name as defined in the configMap for the db-host which is `db-svc`


For the web server, I used a `LoadBalancer` to get an external IP to my web app:

server-svc.yaml:
```yaml
apiVersion: v1
kind: Service
metadata:
  name: server-service
spec:
  type: LoadBalancer
  ports:
    - port: 80
      targetPort: 5000 
      protocol: TCP
      name: http
  selector:
    app: server

```
In my K8s deployment, I have not used the nginx reverse proxy container that handles the client requests like in the docker-compose deployment, so instead it is handled by the loadbalancer service using http port 80. Also, since my application is a flask app it is listening on port 5000.

### Deployments

For the database deployment, I used a statfulset to keep track of the state of my db pods and to ensure order and uniqueness. And I also used a headless service to give the pod's IP address.

postgres-sts.yaml:
```yaml
apiVersion: v1
kind: Service
metadata:
  name: postgres-headless
  labels:
    app: postgres-sts
spec:
  ports:
    - port: 5432
      name: postgres
      targetPort: 5432
  clusterIP: None
  selector:
    app: postgres-sts
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: postgres-sts
spec:
  serviceName: "postgres-headless"
  replicas: 1
  selector:
    matchLabels:
      app: postgres-sts
  template:
    metadata:
      labels:
        app: postgres-sts
    spec:
      initContainers:
        - name: init
          image: alpine
          command: ["sh", "-c", "chown 999:999 /var/lib/postgresql/data"]
          volumeMounts:
            - mountPath: /var/lib/postgresql/data
              name: postgres
      containers:
        - name: postgres
          image: postgres:latest
          securityContext:
            runAsUser: 999
          ports:
            - containerPort: 5432
          env:
            - name: POSTGRES_USER
              valueFrom:
                secretKeyRef:
                  name: secrets
                  key: db-username
            - name: POSTGRES_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: secrets
                  key: db-password
            - name: POSTGRES_DB
              valueFrom:
                configMapKeyRef:
                  name: config-map
                  key: db-name
            - name: PGDATA
              value: /var/lib/postgresql/data/pgdata
          volumeMounts:
            - name: postgres
              #  subPath: pgdata
              mountPath: /var/lib/postgresql/data/
      volumes:
        - name: postgres
          persistentVolumeClaim:
            claimName: azure-managed-disk

```

In order to ensure that we have writing permissions in the postgresql container, the permissions should be changed with the `chown` command:
```yaml
spec:
      initContainers:
        - name: init
          image: alpine
          command: ["sh", "-c", "chown 999:999 /var/lib/postgresql/data"]
          volumeMounts:
            - mountPath: /var/lib/postgresql/data
              name: postgres
```
postgresql has a uid of `999` so we just need to give the user permissions to write in the data before mounting the volume. `initContainers` is run before the app containers are started.


As for the web server deployment, I pushed my image to dockerhub and I made sure to import the configMap and secretsmanifests for every environment variable in the deployment.

server-dep.yaml:
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: server
  labels:
    app: server
spec:
  replicas: 1
  selector:
    matchLabels:
      app: server
  template:
    metadata:
      labels:
        app: server
    spec:
      containers:
        - name: server
          image: siw16/kamstrup-assignment-web
          imagePullPolicy: Always
          ports:
            - containerPort: 5000
          env:
            - name: SECRET
              valueFrom:
                secretKeyRef:
                  name: secrets
                  key: secret
            - name: POSTGRES_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: secrets
                  key: db-password
            - name: POSTGRES_HOST
              valueFrom:
                configMapKeyRef:
                  name: config-map
                  key: db-host
            - name: POSTGRES_PORT
              valueFrom:
                configMapKeyRef:
                  name: config-map
                  key: db-port
            - name: POSTGRES_DB
              valueFrom:
                configMapKeyRef:
                  name: config-map
                  key: db-name
            - name: DEBUG
              valueFrom:
                configMapKeyRef:
                  name: config-map
                  key: debug
            - name: POSTGRES_USER
              valueFrom:
                secretKeyRef:
                  name: secrets
                  key: db-username


```



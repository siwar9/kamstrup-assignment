# kamstrup-assignment

## Marketplace Webapp 🛍️

This assignment will have:
- Marketplace webapp and Dockerfiles to build image
- Docker-compose deployment
- Kubernetes deployment on AKS

## 1. Frontend
- Displays all available products and previously purchased products
- **Technologies:** HTML,CSS,Bootstrap and JavaScript

## 2. Backend
- Communicates with database to get available products in stock in the database through an api that is accessible to Frontend
- Communicates with database to get previously bought items in the database through an api that is accessible to Frontend
- **Technologies:** Python, Flask

## 3. Database
- Is contacted only by Backend
- Dev mode db: Sqlite3
- Prod mode db: PostgreSQL
- **Technologies:** Sqlite3, PostgreSQL (through Flask SQLAlchemy)

At first I started with Sqlite3 only and then switched to PostgreSQL because according to my research it is better in production.

### Overview of endpoints

- `/` is the home where the products are displayed and the users can buy items and review previously bought products.
- `/admin` is the dashboard of the admin 
- `/register` is to register admins only (not users, I did not develop the registration for users because it is a detail)
- `login` is to the login form for the admins
- `/addcategory` where the admin can add a category of products
- `/addbrand` where the admin can add a certain brand 
- `/addproduct` where the admin can add a product 

## Test Code

I did not find the time to configure a pipeline however I am set on doing it very soon. For the meantime, the test codes are available in the `tests` folder located under the `src` folder. I prepared some tests to test the GET/POST requests and the forms (login and register) using mock data.
To test correctly, execute the command `python -m pytest` inside the `tests` folder under `src`

## Deployment

### docker-compose
I have prepared a docker-compose deployment to test the app locally. To run the app use `docker-compose up --build -d`
I made sure to use a persistent volume to make sure that the data will not be lost. Make sure to stop the web app using `docker-compose stop` and not down (takes down the volume as well).
To run the webapp in prod mode, change the `debug` value in `docker-compose` to 0. I included a `demo` (marketplace-assignment.mp4) to showcase the working webapp.


### kubernetes

**Running app on AKS:** http://20.26.169.252/

For the deployment on kubernetes, I used the cloud provider Microsoft Azure for personal financial purposes. I used the AKS or Azure Kubernetes Service and deployed my application successfully. I made sure to use Persistent Volume Claim (PVC) and configured it to a storage class in the cloud provided by Microsoft Azure to really have persistency in the data. For further details, go to the `deployment` folder.










